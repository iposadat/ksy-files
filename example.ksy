meta:
  id: png
  version: 0.1.0
seq:
  - id: magic_number
    type: u32
    le: true
    eq: 0x89504E47

  - id: ihdr
    type: png_ihdr

  - id: idat_chunks
    type: png_idat_chunk
    multiple: true

  - id: iend_chunk
    type: png_iend_chunk
